# Ansible Wazuh Manager Agent list

This playbook is used for listing all agent that is connected to the Wazuh manager

to start the playbook make sure edit the host.ini file to change the IP to your wazuh manager's IP, username and privatekey path for ansible playbook to login into the server (please see sample below)
```
wazuh-manager ansible_host=192.168.56.26 ansible_user=youransibleuser ansible_ssh_private_key_file=yourprivkeypath
```

The last things, make sure that you changed the `ossec_bin_path` in the files `vars.yml` to your ossec binary path so the playbook can lookup binary `agent_control` to perform agent listing

To start the playbook simply run this command

```
ansible-playbook -i hosts.ini playbook.yml
```

and then the sample output should be as follows

```

PLAY [Check wazuh manager] *********************************************************************************************

TASK [Execute agent_control command] ***********************************************************************************
changed: [wazuh-manager]

TASK [Display command output] ******************************************************************************************
ok: [wazuh-manager] => {
    "command_output.stdout_lines": [
        "",
        "Wazuh agent_control. List of available agents:",
        "   ID: 000, Name: wazuh_manager (server), IP: 127.0.0.1, Active/Local",
        "   ID: 001, Name: WIN-Agent-1, IP: any, Active",
        "",
        "List of agentless devices:"
    ]
}

PLAY RECAP *************************************************************************************************************
wazuh-manager              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```